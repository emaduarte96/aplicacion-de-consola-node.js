const fs = require('fs');
const colors = require('colors');


const crearArchivo = async (base, imprimir, tope) => {
    try {
        let salida = '';
        let consola = '';

        for (let i = 1; i <= tope; i++) {
            salida += `${base} x ${i} = ${base * i}\n`;
            consola += `${base} ${`x`.red} ${i} ${`=`.red} ${base * i}\n`;
        }

        if (imprimir) {
            console.log('=========================='.america);
            console.log(`     Tabla del ${base}:    `);
            console.log('=========================='.america);
            console.log(consola);
        }

        fs.writeFileSync(`./salida/tabla-${base}.txt`, salida);


        return `tabla-${base}.txt`;
    } catch (err) {
        throw err;
    }
}


module.exports = {
    crearArchivo,
}